import React, { createContext, useState, useEffect, useCallback } from "react";
import ShowsList from "./ShowsList";
import { queryString } from "../lib";
import Navigation from "./Navigation";

export const AppContext = createContext();

export default () => {
  const [shows, setShows] = useState([]);
  const [total, setTotal] = useState(0);
  const [type, setType] = useState(null);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);

  const fetchShows = useCallback(async () => {
    const query = queryString({ type, _page: page, _limit: limit });
    const showsApi = `${process.env.REACT_APP_API_URL}/shows?${query}`;
    const response = await fetch(showsApi);
    const data = await response.json();

    setTotal(response.headers.get("X-Total-Count"));
    setShows(data);
  }, [type, page, limit]);

  useEffect(() => {
    fetchShows();
  }, [fetchShows]);

  const context = {
    total,
    shows,
    page,
    limit,
    setPage
  };

  return (
    <AppContext.Provider value={context}>
      <Navigation />
      {shows.map((show, i) => (
        <div key={i}>{show.title}</div>
      ))}
    </AppContext.Provider>
  );
};
