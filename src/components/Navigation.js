import React, { useContext } from "react";
import { AppContext } from "./App";
import { pagination } from "../lib";

export default () => {
  const { total, page, limit, setPage } = useContext(AppContext);
  const maxPage = Math.floor(total / limit);
  const pagesArray = pagination(page, maxPage);

  return (
    <div>
      <button onClick={e => setPage(page - 1)} disabled={page < 2}>
        &lt;
      </button>

      {pagesArray.map(pager => (
        <button
          key={pager}
          onClick={e => setPage(pager)}
          disabled={pager === page}
        >
          {pager}
        </button>
      ))}

      <button
        onClick={e => setPage(page + 1)}
        disabled={(page + 1) * limit > total}
      >
        &gt;
      </button>
    </div>
  );
};
