export const queryString = data => {
  return Object.keys(data)
    .filter(key => data[key] !== null)
    .map(key => [key, data[key]].map(encodeURIComponent).join("="))
    .join("&");
};

export const pagination = (page, max) => {
  const delta = 2
  const length = 5
  const start = page - delta > 0 
    ? max < page + delta
      ? max - length + 1 : page - delta
    : 1;
  return Array.from({length: length}, (v, k) => k + start); 
};
